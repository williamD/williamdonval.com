var icon = document.getElementById('menu-icon');
var side = document.getElementById("sidebar");
var focus = document.getElementById("outfocus");
icon.addEventListener("click",function() {
    icon.classList.toggle("active");
    side.classList.toggle("active");
    focus.classList.toggle("active");
});

focus.addEventListener("click",function() {
    icon.classList.remove("active");
    side.classList.remove("active");
    focus.classList.remove("active");
});
