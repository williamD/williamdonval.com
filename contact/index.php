<?php
if(isset($_POST['mailform']))
{
	if(!empty($_POST['nom']) AND !empty($_POST['mail']) AND !empty($_POST['message']))
	{
		$header="MIME-Version: 1.0\r\n";
		$header.='From:"PrimFX.com"<support@primfx.com>'."\n";
		$header.='Content-Type:text/html; charset="uft-8"'."\n";
		$header.='Content-Transfer-Encoding: 8bit';

		$message='
		<html>
			<body>
				<div align="center">
					<img src="http://www.primfx.com/mailing/banniere.png"/>
					<br />
					<u>Nom de l\'expéditeur :</u>'.$_POST['nom'].'<br />
					<u>Mail de l\'expéditeur :</u>'.$_POST['mail'].'<br />
					<br />
					'.nl2br($_POST['message']).'
					<br />
					<img src="http://www.primfx.com/mailing/separation.png"/>
				</div>
			</body>
		</html>
		';

		mail("primfxtuto@gmail.com", "CONTACT - Monsite.com", $message, $header);
		$msg="Votre message a bien été envoyé !";
	}
	else
	{
		$msg="Tous les champs doivent être complétés !";
	}
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>William Donval - Contact</title>
    <link rel="icon" href="../img/favicon.png">
    <link rel="stylesheet" href="../css/style.css">
	<script src="../js/fontawesome-all.min.js"></script>
	<!--<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('g-recaptcha', {
          'sitekey' : 'your_site_key',
		  'theme' : 'dark'
        });
      };
    </script>-->
</head>
<body>
    <div class="contact">
	
	
	<h1 class="page-title">Contact</h1>
	

    <div id="menu-icon" class="cd-nav-trigger">
        <div class="line">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="cross">
            <span></span>
            <span></span>
        </div>
    </div>
        
    <div id="sidebar" class="sidebar">
        <div class="photo"></div>
        <ul>
            <li><a href="../index.html">Accueil</a></li>
            <li><a href="../formation/index.html">Formation</a></li>
            <li><a href="../competences/index.html">Compétences</a></li>
            <li><a href="../projets/index.html">Projets</a></li>
            <li><a href="../experiencepro/index.html">Expérience professionelle</a></li>
            <li class="active"><a href="index.php">Contact</a></li>
        </ul>
        <div class="social">
            <a href="https://www.linkedin.com/in/wdonval/" target="_blank"><i class="fab fa-linkedin"></i></a>
            <a href="https://gitlab.com/williamD" target="_blank"><i class="fab fa-gitlab"></i></a>
        </div>
    </div>
	<div id="outfocus" class="outfocus"></div>
	
		<span class="label">Contactez moi via ce formulaire ou directement en m'envoyant un email à l'adresse : &nbsp;<a href="mailto:contact@williamdonval.com">contact@williamdonval.com</a></span>
		<form method="POST" action="" id="contact">
			<input class="input" type="text" name="nom" placeholder="Votre nom" value="<?php if(isset($_POST['nom'])) { echo $_POST['nom']; } ?>"><br>
			<input class="input" type="email" name="email" placeholder="Votre adresse email" value="<?php if(isset($_POST['mail'])) { echo $_POST['mail']; } ?>"><br>
			<input class="input" type="text" name="sujet" placeholder="Sujet"><br>
			<textarea name="message" placeholder="Votre message" cols="1" rows="10"><?php if(isset($_POST['message'])) { echo $_POST['message']; } ?></textarea><br>
			<!--<div class="g-recaptcha" data-sitekey="6LdOvFEUAAAAAEZymVMWTYbenzA1aB4tCZyP_ZxQ"></div>-->
			<button type="submit" form="contact" value="Submit">Envoyer<i class="fas fa-envelope"></i></button>
		</form>
		
		<?php
		if(isset($msg))
		{
			echo $msg;
		}
		?>
		
	
	</div>
	<footer>
		<img class="wave" src="../img/wave.svg" alt="svg wave">
	</footer>
	<script src="../js/sidebar.js"></script>

</body>
</html>